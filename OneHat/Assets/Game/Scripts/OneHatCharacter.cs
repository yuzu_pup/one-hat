﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using Sirenix.OdinInspector;

public class OneHatCharacter : Character
{
    [BoxGroup("OneHat")]
    public bool Debug_InfiniteHats;
    private bool _HasHat = true;
    [BoxGroup("OneHat"), ShowInInspector]
    public bool HasHat {
        get {
            return _HasHat;
        }
        set {
            _animator.SetBool("HasHat", value);
            _HasHat = value;
        }
    }

    private bool _AboveHat;
    public bool AboveHat {
        get {
            return _AboveHat;
        }
        set {
//            _animator.SetBool("AboveHat", value);
            _AboveHat = value;
        }
    }

    public bool HeadIsBelowObstacle = false;
    private bool _HatIsDirty = false;
    public bool HatIsDirty {
        get {
            return _HatIsDirty;
        }
        set {
            _HatIsDirty = value;
            _animator.SetBool("Dirty", value);
        }
    }

    public HatController HatOnGround;

    [BoxGroup("OneHat")]
    public bool InteractWithHatTimerElapsed = false;
    [BoxGroup("OneHat")]
    public Transform HatBoopRayStartPos;
    [BoxGroup("OneHat")]
    public GameObject HatPrefab;
    [BoxGroup("OneHat")]
    public float BoopForce = 50;

    public float InteractPreventTime = 0.5f;

    public Action OnGoalTouched;
	protected override void Initialization() {
        base.Initialization();
        _animator.SetBool("HasHat", true);
    }

    public void LostHat() {
        HasHat = false;
        InteractWithHatTimerElapsed = false;
        StartCoroutine(StartHatIsPickupableTimer(InteractPreventTime));
    }

    protected override void Update() {
        base.Update();
        float offsetFromCenter = Mathf.Abs(HatBoopRayStartPos.localPosition.x);
        RaycastHit2D aboveHeadHit = MMDebug.RayCast(HatBoopRayStartPos.position, IsFacingRight ? Vector2.right : Vector2.left, offsetFromCenter*2, LayerMask.GetMask("Platforms"), Color.red, true);
        HeadIsBelowObstacle = aboveHeadHit;
        if(HasHat) {
            if(aboveHeadHit) {
                SpawnHatAt(HatBoopRayStartPos.position).Throw(IsFacingRight ? -BoopForce : BoopForce, 50);
            }
        } else {
            RectTransform rt = transform as RectTransform;
            RaycastHit2D hit = MMDebug.RayCast(new Vector2(rt.position.x + rt.rect.width/4, rt.position.y + rt.rect.height/2), Vector2.down, rt.rect.height, LayerMask.GetMask("NotPlayer"), Color.green, true);
            RaycastHit2D hit2 = MMDebug.RayCast(new Vector2(rt.position.x - rt.rect.width/4, rt.position.y + rt.rect.height/2), Vector2.down, rt.rect.height, LayerMask.GetMask("NotPlayer"), Color.green, true);
            RaycastHit2D hit3 = MMDebug.RayCast(new Vector2(rt.position.x, rt.position.y + rt.rect.height/2), Vector2.down, rt.rect.height, LayerMask.GetMask("NotPlayer"), Color.green, true);
            AboveHat = hit || hit2;
            if(AboveHat) {
                if(hit) {
                    HatOnGround = hit.collider.gameObject.GetComponent<HatController>();
                }
                if(hit2) {
                    HatOnGround = hit2.collider.gameObject.GetComponent<HatController>();
                }
                if(hit3) {
                    HatOnGround = hit3.collider.gameObject.GetComponent<HatController>();
                }
            } else {
                HatOnGround = null;
            }
        }
    }
    public HatController SpawnHatAt(Vector3 pos) {
        LostHat();
        GameObject hatInst = GameObject.Instantiate(HatPrefab);
        hatInst.transform.position = pos;
        HatController hatCont = hatInst.GetComponent<HatController>();
        if(hatCont == null) {
            throw new System.Exception("Forgot to put a hat controller on hat prefab");
        }
        hatCont.Dirty = HatIsDirty;
        return hatCont;
    }

    public bool CanInteractWithHat() {
        return InteractWithHatTimerElapsed && HatOnGround != null;
    }

    public bool CanPickUpHat() {
        return CanInteractWithHat() && !HeadIsBelowObstacle;
    }

    public void PickUpHat() {
        HatIsDirty = HatOnGround.Dirty;
        GameObject.Destroy(HatOnGround.gameObject);
        HasHat = true;
    }
    
    private IEnumerator StartHatIsPickupableTimer(float time) {
        yield return new WaitForSeconds(time);
        InteractWithHatTimerElapsed = true;   
    }


}
