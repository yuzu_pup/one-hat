﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalTile : PlayerAndHatAffectingTile
{
    public Animator _Animator;
    public float AnimationTime;
    public bool CanProceedToNextLevel;
    public GameObject GoalUI;

    void Start() {
        OnPlayerEntered = OnPlayerStay = (ohc) => {
            if(ohc.HasHat) {
                if(ohc.HatIsDirty) {
                    _Animator.SetTrigger("Sad");
                } else {
                    _Animator.SetBool("Happy", true);
                    ohc.Disable();
                    ohc._animator.SetTrigger("Victory");
                    GoalUI.SetActive(true);
                    StartCoroutine(AnimationTimer());
                }
            } else {
                _Animator.SetTrigger("WhereHat");
            }
        };
    }

    void Update() {
        if(Input.anyKey && CanProceedToNextLevel) {
            SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    IEnumerator AnimationTimer() {
        yield return new WaitForSeconds(AnimationTime);
        CanProceedToNextLevel = true;
    }
}
