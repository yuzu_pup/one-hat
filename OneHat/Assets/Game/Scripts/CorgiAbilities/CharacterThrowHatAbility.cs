﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

[RequireComponent(typeof(OneHatCharacter))]
public class CharacterThrowHatAbility : CharacterAbility
{
    private OneHatCharacter _oneHatCharacter;
    
    public float HorizontalThrowForce = 500;
    public float VerticalThrowForce = 350;
    public Transform HatThrowPosition;

    protected override void Initialization() {
        base.Initialization();
        _oneHatCharacter = _character as OneHatCharacter;
    }

    protected override void HandleInput() 
    {
        if(_inputManager.DashButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
            if(_oneHatCharacter.HasHat || _oneHatCharacter.Debug_InfiniteHats) {
                ThrowHat();
            }
        }
    }

    private void ThrowHat() {
        _oneHatCharacter.SpawnHatAt(HatThrowPosition.position).Throw(_character.IsFacingRight ? HorizontalThrowForce : -HorizontalThrowForce, VerticalThrowForce);
    }
}
