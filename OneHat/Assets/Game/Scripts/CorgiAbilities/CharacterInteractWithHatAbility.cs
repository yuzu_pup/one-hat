﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

[RequireComponent(typeof(OneHatCharacter))]
public class CharacterInteractWithHatAbility : CharacterAbility
{
    private OneHatCharacter _oneHatCharacter;

    public float KickForceHorizontal;
    public float KickForceVertical;    

    protected override void Initialization() {
        base.Initialization();
        _oneHatCharacter = _character as OneHatCharacter;
    }

    protected override void HandleInput() 
    {
        if(_inputManager.DashButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
            if(!_oneHatCharacter.HasHat) {
                if(_oneHatCharacter.CanInteractWithHat()) {
                    if(!_oneHatCharacter.HeadIsBelowObstacle) {
                        Debug.Log("Pick up");
                        _oneHatCharacter.PickUpHat();
                    } else {
                        Debug.Log("Kick");
                        KickHat(_oneHatCharacter.HatOnGround);                    
                    }
                }
            }
        }
    }

    protected void KickHat(HatController hat) {
        hat._Rigidbody2D.AddForce(new Vector2(_oneHatCharacter.IsFacingRight ? KickForceHorizontal : -KickForceHorizontal, KickForceVertical));
        hat.Dirty = true;
        _oneHatCharacter.HatIsDirty = true;
    }
}