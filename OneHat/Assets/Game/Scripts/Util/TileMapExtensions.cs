﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class TileMapExtensions
{
    public static TileBase GetTileAtWorldPosition(this Tilemap tilemap, Vector3 worldPos) {
        Grid grid = tilemap.layoutGrid;
        Vector3Int gridPos = grid.WorldToCell(worldPos);
        return tilemap.GetTile(gridPos);
    }
}
