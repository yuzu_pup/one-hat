﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class AutoFitB2DCToSprite : MonoBehaviour
{
    [Button("Fit")]
    public void FitBoxCollider2DToObj() {
        BoxCollider2D col = GetComponent<BoxCollider2D>();

        RectTransform rt = transform as RectTransform;
        col.offset = Vector2.zero;
        col.size = new Vector2(rt.rect.width, rt.rect.height);
    }   
}
