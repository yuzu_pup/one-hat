﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadSceneOnButtonPress : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R)) {
            Debug.Log("reload");
            SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
        }
    }
}
