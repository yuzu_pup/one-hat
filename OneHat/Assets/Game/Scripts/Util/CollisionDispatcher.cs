﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnCollisionEnter2DDel(GameObject me, Collision2D other);
public delegate void OnTriggerEnter2DDel(GameObject me, Collider2D other);

public class CollisionDispatcher : MonoBehaviour
{
    public OnCollisionEnter2DDel OnCollissionEnter2DEvent;
    public OnTriggerEnter2DDel OnTriggerEnter2DEvent;

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("Collision2D with: " + other.gameObject.name);
        if(OnCollissionEnter2DEvent != null) {
            OnCollissionEnter2DEvent(this.gameObject, other);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Trigger2D");
        if(OnTriggerEnter2DEvent != null) {
            OnTriggerEnter2DEvent(this.gameObject, other);
        }
    }
}
