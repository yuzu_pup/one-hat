﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class WindBlock : PlayerAndHatAffectingTile
{
    public float Force = 800f;
    public float FloatForce = 65f;
    private Vector2 _ForceVectorL;
    private Vector2 _ForceVectorR;

    [Button("Flip Direction")]
    void Flip() {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    void Start() {
        _ForceVectorL = new Vector2(-Force, 0);
        _ForceVectorR = new Vector2(Force, 0);
        // OnHatEntered = (hc) => hc._Rigidbody2D.AddForce(transform.localScale.x > 0 ? _ForceVectorR : _ForceVectorL);
        OnHatStay = (hc) => hc._Rigidbody2D.AddForce(new Vector2(transform.localScale.x > 0 ? Force : -Force, FloatForce));
    }
}
