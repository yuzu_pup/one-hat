﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class HatController : MonoBehaviour
{
    public Animator _Animator;
    public Rigidbody2D _Rigidbody2D;
    public BoxCollider2D _BoxCollider2D;

    private bool _Dirty = false;
    public bool Dirty {
        get {
            return _Dirty;
        }
        set {
            _Dirty = value;
            _Animator.SetBool("Dirty", value);
        }
    }

    private bool _Flying;
    public bool Flying {
        get {
            return _Flying;
        }
        set {
            _Flying = value;
            _Animator.SetBool("Flying", value);
        }
    }

    [Button, HideInEditorMode]
    public void Throw(float horizontalThrowForce, float verticalThrowForce) {
        _Rigidbody2D.AddForce(new Vector2(horizontalThrowForce, verticalThrowForce));
        Flying = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("hat trigger: " + other.name);
        if(other.gameObject.layer == LayerMask.NameToLayer("Platforms")) {
            Flying = false;
        }        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("hat colission: " + other.gameObject.name);
        if(other.gameObject.layer == LayerMask.NameToLayer("Platforms")) {
            Flying = false;
        }        
    }

    public void Freeze() {
        Flying = false;
        _Rigidbody2D.isKinematic = true;
        _Rigidbody2D.gravityScale = 0;
        _Rigidbody2D.velocity = Vector2.zero;
        _Rigidbody2D.angularVelocity = 0f;
        _Rigidbody2D.rotation = 0f;
    }
}
