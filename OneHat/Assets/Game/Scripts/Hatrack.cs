﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hatrack : MonoBehaviour
{
    public Transform HatPos; 
    public bool HasHat;
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        HatController hatController = other.GetComponent<HatController>();
        if(hatController != null) {
            hatController.Freeze();
            hatController.transform.position = HatPos.position;
        }
    }
}
