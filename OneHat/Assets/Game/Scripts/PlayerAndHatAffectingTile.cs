﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAndHatAffectingTile : MonoBehaviour
{
    public System.Action<OneHatCharacter> OnPlayerEntered;
    public System.Action<OneHatCharacter> OnPlayerStay;
    public System.Action<HatController> OnHatEntered;
    public System.Action<HatController> OnHatStay;
    // public System.Action OnHatStay;
    // public System.Action OnHatLeave;
    public bool AffectPlayer = true;
    public bool AffectHat = true;

    void OnTriggerEnter2D(Collider2D other)
    {
        HatController hc = other.GetComponent<HatController>();
        if(hc && AffectHat) {
            if(OnHatEntered != null) {
                OnHatEntered(hc);
            }
        }        

        OneHatCharacter ohc = other.GetComponent<OneHatCharacter>();
        if(ohc && AffectPlayer) {
            if(OnPlayerEntered != null) {
                OnPlayerEntered(ohc);
            }
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        OneHatCharacter ohc = other.GetComponent<OneHatCharacter>();
        if(ohc && AffectPlayer) {
            if(OnPlayerStay != null) {
                OnPlayerStay(ohc);
            }
        }
        
        HatController hc = other.GetComponent<HatController>();
        if(hc && AffectHat) {
            if(OnHatStay != null) {
                OnHatStay(hc);
            }
        }        
    }
}
