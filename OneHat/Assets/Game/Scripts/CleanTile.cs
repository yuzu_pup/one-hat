﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanTile : PlayerAndHatAffectingTile
{
    void Start() {
        OnHatEntered = (hc) => {
            hc.Dirty = false;
        };
        OnPlayerEntered = (ohc) => {
            if(ohc.HasHat) {
                ohc.HatIsDirty = false;
            }
        };
    }
}
