﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtyTile : PlayerAndHatAffectingTile
{
    void Start() {
        OnHatEntered = (hc) => {
            hc.Dirty = true;
        };
        OnPlayerEntered = (ohc) => {
            if(ohc.HasHat) {
                ohc.HatIsDirty = true;
            }
        };
    }
}
